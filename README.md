<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.0.0, < 2.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_subnet.subnet_generic](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assign_ipv6_on_creation"></a> [assign\_ipv6\_on\_creation](#input\_assign\_ipv6\_on\_creation) | specify true to indicate that network interfaces created in the specified subnet should be assigned an IPv6 address, default: false | `bool` | `false` | no |
| <a name="input_subnet_az"></a> [subnet\_az](#input\_subnet\_az) | availability zone the subnet should be in | `string` | n/a | yes |
| <a name="input_subnet_enable_a_records"></a> [subnet\_enable\_a\_records](#input\_subnet\_enable\_a\_records) | indicates whether to respond to DNS queries for instance hostnames with DNS A records, default: false | `bool` | `false` | no |
| <a name="input_subnet_enable_aaaa_records"></a> [subnet\_enable\_aaaa\_records](#input\_subnet\_enable\_aaaa\_records) | indicates whether to respond to DNS queries for instance hostnames with DNS AAAA records, default: false | `bool` | `false` | no |
| <a name="input_subnet_enable_dns64"></a> [subnet\_enable\_dns64](#input\_subnet\_enable\_dns64) | indicates whether DNS queries made to Amazon DNS should return synthetic IPv6 addresses for IPv4-only destinations, default: false | `bool` | `false` | no |
| <a name="input_subnet_ipv4_cidr_block"></a> [subnet\_ipv4\_cidr\_block](#input\_subnet\_ipv4\_cidr\_block) | specify the IPv4 CIDR block for the subnet | `string` | n/a | yes |
| <a name="input_subnet_ipv6_cidr_block"></a> [subnet\_ipv6\_cidr\_block](#input\_subnet\_ipv6\_cidr\_block) | specify the IPv6 CIDR block for the subnet, default: fd12:3456:789a:1::/64 (private range, RFC4193) | `string` | `"fd12:3456:789a:1::/64"` | no |
| <a name="input_subnet_ipv6_only"></a> [subnet\_ipv6\_only](#input\_subnet\_ipv6\_only) | indicates whether the subnet is IPv6-only, default: false | `bool` | `false` | no |
| <a name="input_subnet_map_public_ips"></a> [subnet\_map\_public\_ips](#input\_subnet\_map\_public\_ips) | indicates whether instances in the subnet will get public IP addresses at launch, default: false | `bool` | `false` | no |
| <a name="input_subnet_tags"></a> [subnet\_tags](#input\_subnet\_tags) | a map of key-value pairs consisting of additional tags to set on the subnet | `map(string)` | `{}` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID that the subnet will be located in | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_subnet_arn"></a> [subnet\_arn](#output\_subnet\_arn) | Amazon resource name of the subnet |
| <a name="output_subnet_id"></a> [subnet\_id](#output\_subnet\_id) | ID of the subnet |
| <a name="output_subnet_ipv6_assoc_id"></a> [subnet\_ipv6\_assoc\_id](#output\_subnet\_ipv6\_assoc\_id) | the association ID for the IPv6 CIDR block |
| <a name="output_subnet_owner_id"></a> [subnet\_owner\_id](#output\_subnet\_owner\_id) | the ID of the AWS account that owns the subnet |
| <a name="output_subnet_tags"></a> [subnet\_tags](#output\_subnet\_tags) | all tags for this subnet |
<!-- END_TF_DOCS -->