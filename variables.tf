#===========================
# REQUIRED
#===========================
variable "vpc_id" {
  type        = string
  description = "VPC ID that the subnet will be located in"
}
#===========================
# OPTIONAL
#===========================
variable "subnet_az" {
  type        = string
  description = "availability zone the subnet should be in"
}
variable "subnet_tags" {
  type        = map(string)
  description = "a map of key-value pairs consisting of additional tags to set on the subnet"
  default     = {}
}
variable "subnet_ipv4_cidr_block" {
  type        = string
  description = "specify the IPv4 CIDR block for the subnet"
}
variable "subnet_enable_a_records" {
  type        = bool
  description = "indicates whether to respond to DNS queries for instance hostnames with DNS A records, default: false"
  default     = false
}
#variable "subnet_ipv6_cidr_block" {
#  type        = string
#  description = "specify the IPv6 CIDR block for the subnet, default: fd12:3456:789a:1::/64 (private range, RFC4193)"
#  default     = "fd12:3456:789a:1::/64"
#}
#variable "assign_ipv6_on_creation" {
#  type        = bool
#  description = "specify true to indicate that network interfaces created in the specified subnet should be assigned an IPv6 address, default: false"
#  default     = false
#}
#variable "subnet_enable_dns64" {
#  type        = bool
#  description = "indicates whether DNS queries made to Amazon DNS should return synthetic IPv6 addresses for IPv4-only destinations, default: false"
#  default     = false
#}
#variable "subnet_enable_aaaa_records" {
#  type        = bool
#  description = "indicates whether to respond to DNS queries for instance hostnames with DNS AAAA records, default: false"
#  default     = false
#}
#variable "subnet_ipv6_only" {
#  type        = bool
#  description = "indicates whether the subnet is IPv6-only, default: false"
#  default     = false
#}
variable "subnet_map_public_ips" {
  type        = bool
  description = "indicates whether instances in the subnet will get public IP addresses at launch, default: false"
  default     = false
}
