resource "aws_subnet" "subnet_generic" {
  vpc_id            = var.vpc_id
  availability_zone = var.subnet_az
  tags              = var.subnet_tags

  #ipv4
  cidr_block                                  = var.subnet_ipv4_cidr_block
  enable_resource_name_dns_a_record_on_launch = var.subnet_enable_a_records

  #ipv6
  #ipv6_cidr_block                                = var.subnet_ipv6_cidr_block
  #assign_ipv6_address_on_creation                = var.assign_ipv6_on_creation
  #enable_dns64                                   = var.subnet_enable_dns64
  #enable_resource_name_dns_aaaa_record_on_launch = var.subnet_enable_aaaa_records
  #ipv6_native                                    = var.subnet_ipv6_only

  #general network
  map_public_ip_on_launch = var.subnet_map_public_ips
}
