output "subnet_arn" {
  value       = aws_subnet.subnet_generic.arn
  description = "Amazon resource name of the subnet"
}
output "subnet_id" {
  value       = aws_subnet.subnet_generic.id
  description = "ID of the subnet"
}
#output "subnet_ipv6_assoc_id" {
#  value       = aws_subnet.subnet_generic.ipv6_cidr_block_association_id
#  description = "the association ID for the IPv6 CIDR block"
#}
output "subnet_owner_id" {
  value       = aws_subnet.subnet_generic.owner_id
  description = "the ID of the AWS account that owns the subnet"
}
output "subnet_tags" {
  value       = aws_subnet.subnet_generic.tags_all
  description = "all tags for this subnet"
}
